#!/bin/bash

ROOT='http://169.254.169.254/'
substring="/"

traverse () {
  for file in $(curl $ROOT$1 -s); do
    if test "${file#*$substring}" != "$string"; then
      curl -s $ROOT$1$file -o $1$file --create-dirs
    else
      traverse $1$file
    fi
  done
}

traverse 'latest/meta-data/'

cat latest/meta-data/iam/security-credentials/* | grep 'AccessKeyId\|SecretAccessKey\|Token'
