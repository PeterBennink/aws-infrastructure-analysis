# AWS Infrastructure Analysis


## Prerequisites

 - Python 3
 - boto3
 - neo4j
 - py2neo

## Instructions

`metadata-crawl.sh` is to be ran on an EC2 instance. It will store all metadata in folder 'latest', and output the found credentials on screen.

`get-infrastructure.py` will try to connect to a local Neo4j instance via Bolt. Currently username `neo4j` is hardcoded, so if yours is different you should change that in the script.
Password will be asked for on running the tool. All AWS credentials used need to be in a file called 'credentials' in the same folder as the script.

1. Install prerequisites
2. (optional) Run `metadata-crawl.sh` on EC2 instance.
3. Create file called 'credentials' in same folder as `get-infrastructure.py`
4. Run `python3 get-infrastructure.py`
