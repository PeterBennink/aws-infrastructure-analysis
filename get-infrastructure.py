import getpass
import boto3
from flatten_json import flatten
from py2neo import Graph, Node, Relationship, NodeMatcher, RelationshipMatcher

client = None
access_key = None
secret_access_key = None
token = None
p = getpass.getpass('Neo4j password:')
graph = Graph("bolt://localhost:7687", auth=("neo4j", p))
nodematcher = NodeMatcher(graph)
relmatcher = RelationshipMatcher(graph)
graph.schema.create_uniqueness_constraint("EC2", "InstanceId")
graph.schema.create_uniqueness_constraint("RDS", "DBInstanceIdentifier")
graph.schema.create_uniqueness_constraint("S3", "Name")
graph.schema.create_uniqueness_constraint("VPC", "VpcId")
graph.schema.create_uniqueness_constraint("SG", "GroupId")
graph.schema.create_uniqueness_constraint("Volume", "Ebs_VolumeId")
graph.schema.create_uniqueness_constraint("Credentials", "access_key")


def iterate_credentials():
    global access_key, secret_access_key, token
    text_file = open("credentials", "r")
    credentials = text_file.read().splitlines()
    for i in credentials:
        [access_key, secret_access_key, token] = i.split(',')
        if not token:
            token = None
        retrieve_data()
    for i in credentials:
        [access_key, secret_access_key, token] = i.split(',')
        if not token:
            token = None
        keys = {'access_key': i.split(',')[0],
                'secret_access_key': i.split(',')[1],
                'token': i.split(',')[2]}
        credential = nodematcher.match("Credentials", **keys).first()
        if not credential:
            credential = Node("Credentials", **keys)
            graph.create(credential)
        bruteforce_permissions(credential)


def bruteforce_permissions(credential):
    bruteforcers = (bruteforce_ec2_permissions, bruteforce_rds_permissions, bruteforce_s3_permissions)
    for bruteforcer in bruteforcers:
        bruteforcer(credential)


def bruteforce_ec2_permissions(credential):
    methods = [('AssociateVpcCidrblock', 'associate_vpc_cidrblock', True, 'VPC'),
               ('CreateDefaultVpc', 'create_default_vpc', True, 'VPC'),
               ('DescribeImages', 'describe_images', True, 'EC2'),
               ('DescribeSecurityGroups', 'describe_security_groups', False, 'SG'),
               ('DescribeVolumes', 'describe_volumes', False, 'Volume'),
               ('DescribeVolumesModifications', 'describe_volumes_modifications', False, 'Volume'),
               ('DescribeVolumeStatus', 'describe_volume_status', False, 'Volume'),
               ('DescribeVpcClassicLink', 'describe_vpc_classic_link', False, 'VPC'),
               ('DescribeVpcClassicLinkDnsSupport', 'describe_vpc_classic_link_dns_support', False, 'VPC'),
               ('DescribeVpcEndpointConnectionNotifications', 'describe_vpc_endpoint_connection_notifications', False, 'VPC'),
               ('DescribeVpcEndpointConnections', 'describe_vpc_endpoint_connections', False, 'VPC'),
               ('DescribeVpcEndpoints', 'describe_vpc_endpoints', False, 'VPC'),
               ('DescribeVpcEndpointServiceConfigurations', 'describe_vpc_endpoint_service_configurations', False, 'VPC'),
               ('DescribeVpcEndpointServices', 'describe_vpc_endpoint_services', False, 'VPC'),
               ('DescribeVpcPeeringConnections', 'describe_vpc_peering_connections', False, 'VPC'),
               ('DescribeVpcs', 'describe_vpcs', False, 'VPC')]

    return generic_permission_bruteforcer('ec2', methods, credential)


def bruteforce_rds_permissions(credential):
    methods = [('DescribeDBInstances', 'describe_db_instances', False, 'RDS'),
               ('DescribeAccountAttributes', 'describe_account_attributes', False, 'RDS'),
               ('DescribeCertificates', 'describe_certificates', False, 'RDS'),
               ('DescribeDBClusterSnapshots', 'describe_db_cluster_snapshots', False, 'RDS'),
               ('DescribeDBInstances', 'describe_db_instances', False, 'RDS'),
               ('DescribeEventCategories', 'describe_event_categories', False, 'RDS'),
               ('DescribeEvents', 'describe_events', False, 'RDS'),
               ('DownloadCompleteDBLogFile', 'download_complete_db_logFile', True, 'RDS'),
               ]

    return generic_permission_bruteforcer('rds', methods, credential)


def bruteforce_s3_permissions(credential):
    methods = [('ListBuckets', 'list_buckets', False, 'S3')]
    return generic_permission_bruteforcer('s3', methods, credential)


def generic_permission_bruteforcer(service, methods, credential):
    global client
    client = boto3.client(service,
                          aws_access_key_id=access_key,
                          aws_secret_access_key=secret_access_key,
                          aws_session_token=token)

    for api_action, method_name, dry_run, entity in methods:
        try:
            getattr(client, method_name)(DryRun=dry_run)
        except Exception as e:
            if hasattr(e, 'response') and e.response['Error'].get('Code') == 'DryRunOperation':
                for i in nodematcher.match(entity).__iter__():
                    rel = relmatcher.match((credential, i)).first()
                    print(rel)
                    try:
                        graph.create(Relationship(credential, "CAN_ACCESS", i))
                    except Exception as d:
                        print(d)
            if e.__class__.__name__ is 'ParamValidationError':
                try:
                    getattr(client, method_name)()
                except Exception as e:
                    print(api_action)
                else:
                    for i in nodematcher.match(entity).__iter__():
                        rel = relmatcher.match((credential, i)).first()
                        print(rel)
                        try:
                            graph.create(Relationship(credential, "CAN_ACCESS", i))
                        except Exception as d:
                            print(d)
        else:
            for i in nodematcher.match(entity).__iter__():
                rel = relmatcher.match((credential, i)).first()
                print(rel)
                try:
                    graph.create(Relationship(credential, "CAN_ACCESS", i))
                except Exception as d:
                    print(d)


def retrieve_data():
    methods = [('ec2', 'describe_vpcs'),
               ('ec2', 'describe_security_groups'),
               ('rds', 'describe_db_instances'),
               ('ec2', 'describe_instances'),
               ('s3', 'list_buckets'),
               # ('s3', 'get_bucket_location'),
    ]

    # if check_root_account(access_key, secret_key, token):
    #     return
    #
    # success, permissions = check_via_iam(access_key, secret_key, token)
    # if success:
    #     print(permissions)
    #     return

    for method in methods:
        try:
            data = run_command(method)
        except Exception:
            continue
        else:
            eval('import_' + method[1] + '(data)')


def run_command(command):
    global client
    client = boto3.client(command[0],
                          aws_access_key_id=access_key,
                          aws_secret_access_key=secret_access_key,
                          aws_session_token=token)

    try:
        output = getattr(client, command[1])()
    except Exception as e:
        print('%s, error: "%s"' % (command[1], e))

    return output


def import_describe_instances(array):
    for instance in array["Reservations"]:
        flat_instance = flatten(instance["Instances"][0])
        instance_node = nodematcher.match("EC2", **flat_instance).first()
        if not instance_node:
            instance_node = Node("EC2", **flat_instance)
            graph.create(instance_node)
        for volume in instance["Instances"][0]["BlockDeviceMappings"]:
            flat_volume = flatten(volume)
            volume_node = nodematcher.match("Volume", **flat_volume).first()
            if not volume_node:
                volume_node = Node("Volume", **flat_volume)
                graph.create(volume_node)
            graph.create(Relationship(instance_node, "HAS_MOUNTED", volume_node))
        for sg in instance["Instances"][0]["SecurityGroups"]:
            sg_node = nodematcher.match("SG", GroupId=sg["GroupId"]).first()
            if not sg_node:
                sg_node = Node("SG", GroupId=sg["GroupId"])
                graph.create(sg_node)
            graph.create(Relationship(sg_node, "CONTAINS", instance_node))


def import_describe_db_instances(array):
    for instance in array["DBInstances"]:
        flat_instance = flatten(instance)
        instance_node = nodematcher.match("RDS", **flat_instance).first()
        if not instance_node:
            instance_node = Node("RDS", **flat_instance)
            graph.create(instance_node)
        for sg in instance["VpcSecurityGroups"]:
            sg_node = nodematcher.match("SG", GroupId=sg["VpcSecurityGroupId"]).first()
            if not sg_node:
                sg_node = Node("SG", GroupId=sg["VpcSecurityGroupId"])
                graph.create(sg_node)
            graph.create(Relationship(sg_node, "CONTAINS", instance_node))


def import_describe_security_groups(array):
    for group in array["SecurityGroups"]:
        flat_group = flatten(group)
        sg = nodematcher.match("SG", **flat_group).first()
        if not sg:
            sg = Node("SG", **flat_group)
            graph.create(sg)
        vpc_node = nodematcher.match("VPC", VpcId=sg.get('VpcId')).first()
        if not vpc_node:
            vpc_node = Node("VPC", VpcId=sg.get('VpcId'))
            graph.create(vpc_node)
        graph.create(Relationship(vpc_node, "CONTAINS", sg))


def import_describe_vpcs(array):
    for vpc in array["Vpcs"]:
        flat_vpc = flatten(vpc)
        vpc_node = nodematcher.match("VPC", **flat_vpc).first()
        if not vpc_node:
            vpc_node = Node("VPC", **flat_vpc)
            graph.create(vpc_node)


def import_list_buckets(array):
    for bucket in array["Buckets"]:
        flat_bucket = flatten(bucket)
        bucket_node = nodematcher.match("S3", **flat_bucket).first()
        if not bucket_node:
            bucket_node = Node("S3", **flat_bucket)
            graph.create(bucket_node)


iterate_credentials()
